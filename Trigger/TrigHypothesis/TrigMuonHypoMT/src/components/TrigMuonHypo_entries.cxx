#include "src/TrigMufastHypoAlg.h"
#include "src/TrigMufastHypoTool.h"
#include "src/TrigmuCombHypoAlg.h"
#include "src/TrigmuCombHypoTool.h"
#include "src/TrigMuisoHypoAlg.h"
#include "src/TrigMuonEFTrackIsolationHypoAlg.h"
#include "src/TrigMuonEFTrackIsolationHypoTool.h"
#include "src/TrigMuonEFMSonlyHypoAlg.h"
#include "src/TrigMuonEFMSonlyHypoTool.h"
#include "src/TrigMuonEFCombinerHypoAlg.h"
#include "src/TrigMuonEFCombinerHypoTool.h"

DECLARE_COMPONENT( TrigMufastHypoAlg )
DECLARE_COMPONENT( TrigMufastHypoTool )
DECLARE_COMPONENT( TrigmuCombHypoAlg )
DECLARE_COMPONENT( TrigmuCombHypoTool )
DECLARE_COMPONENT( TrigMuisoHypoAlg )
DECLARE_COMPONENT( TrigMuisoHypoTool )
DECLARE_COMPONENT( TrigMuonEFTrackIsolationHypoAlg )
DECLARE_COMPONENT( TrigMuonEFTrackIsolationHypoTool )
DECLARE_COMPONENT( TrigMuonEFMSonlyHypoAlg )
DECLARE_COMPONENT( TrigMuonEFMSonlyHypoTool )
DECLARE_COMPONENT( TrigMuonEFCombinerHypoAlg )
DECLARE_COMPONENT( TrigMuonEFCombinerHypoTool )

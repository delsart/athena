#include "TrigFTKBankGen/FTKBankGenAlgo.h"
#include "TrigFTKBankGen/FTKConstGenAlgo.h"

#include "TrigFTKBankGen/PattMergeRootAlgo.h"

#include "TrigFTKBankGen/FTKPattGenRootAlgo.h"

#include "TrigFTKBankGen/FTKCachedBankGenAlgo.h"

DECLARE_COMPONENT( FTKBankGenAlgo )
DECLARE_COMPONENT( FTKConstGenAlgo )

DECLARE_COMPONENT( PattMergeRootAlgo )

DECLARE_COMPONENT( FTKPattGenRootAlgo )

DECLARE_COMPONENT( FTKCachedBankGenAlgo )

